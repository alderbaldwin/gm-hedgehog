// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scrGettingLost(){
	
	//lostPlaceX = x;
	//lostPlaceY = y;
	//placeIsFree = false;


		if x < -160 or x > room_width+160 or y < -160 or y > room_height+160 then {
	        lostPlaceX  = random(room_width); lostPlaceY  = random(room_height); lostMessageActive = true;
	}

		if place_free(lostPlaceX+70, lostPlaceY-70)
		and place_free(lostPlaceX-70, lostPlaceY+70)
		and place_free(lostPlaceX+70, lostPlaceY+70)
		and place_free(lostPlaceX-70, lostPlaceY-70) {
			placeIsFree = true;
		}

	if placeIsFree = true and lostMessageActive = true {
			x = lostPlaceX; y = lostPlaceY;
	        instance_create_depth(x,y,-10000,objLostText) global.timePassed++ show_debug_message("time passed++");	
			lostMessageActive = false;
			placeIsFree = false;
	    }

}


