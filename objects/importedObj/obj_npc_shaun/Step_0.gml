/// @description  check if player is by npc

if (place_meeting(x,y,obj_player))

{
    by_npc = 1;
}

else

{
    by_npc = 0;
}


/// check if player is pressing talk button

if (by_npc = 1) && (global.key_interact)

{
    show_text = 1;
}

else

{
   show_text = 0;
}


