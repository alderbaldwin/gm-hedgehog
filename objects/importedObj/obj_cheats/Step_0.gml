/// @description cheat codes
if keyboard_check(vk_backspace) {
    cheat = get_string("Please enter cheat code!","")
    // if you want explanation text inside of the textbox don't leave it empty above

    
    if cheat = "ladder" {
        global.ladder_climb = true
    }
    
    else if cheat = "restart" {
        game_restart()
    }
    
    else if cheat = "speedup" {
        obj_player.movespeed +=1
    }

    else if cheat = "rocketdodo" {
        obj_player.movespeed +=8
    }    
        
    else if cheat = "slowdown" {
        obj_player.movespeed -=5
    }        
    
    else if cheat = "highjump" {
        obj_player.jumpspeed +=1
    }        

    else if cheat = "doublejump" {
        //global.jumpsmax +=1
        global.double_jump = true;
    }                    
    
    else if cheat = "lowgrav" {
        obj_player.grav +=0.1
    }       
    
    else if cheat = "mail" {
        //obj_management.lettercount+=1
    }  
    
        else if cheat = "orb" {
        global.orb_big +=1;
    } 
    
        else if cheat = "money" {
        global.moneys +=1;
    } 
}





