/// @description SPM: "I am a signpost"

draw_self();

if (showtext == 1)
{   
    //shadow text color
    draw_set_color(c_black); //make text black
    draw_set_halign(fa_center);
    draw_set_font(fnt_default);
    draw_text (x+1,y-64+1, string_hash_to_newline("I am a sign.#I tell facts.")) //text is slightly offset to create shadow

    //top text color
    draw_set_color(make_color_rgb(200,0,200)); //make text pink
    draw_set_halign(fa_center);
    draw_set_font(fnt_default);
    draw_text (x,y-64, string_hash_to_newline("I am a sign.#I tell facts."))
}

