/// @description show button sprite


if (by_npc = 1) and (show_text = 0)
{   
    //show spacebar
    draw_sprite(spr_button_b,0,x,y-64);
}

///person: orbs

draw_self();

if (show_text = 1)
{   
    draw_set_color(c_purple);
    draw_roundrect(x-66,y-110,x+114,y-50,false);

    //shadow text color
    draw_set_color(c_black); //make text black
    draw_set_halign(fa_center);
    draw_set_font(fnt_default);
    draw_text (x+24,y-96, string_hash_to_newline("You have " + string(global.orb_big) + " orbs")) //text is slightly offset to create shadow

    //top text color
    draw_set_color(c_white); //make text white
    draw_set_halign(fa_center);
    draw_set_font(fnt_default);
    draw_text (x+25,y-97, string_hash_to_newline("You have " + string(global.orb_big) + " orbs"))
}

