/// @description  If player touching door - show text or go in
if (place_meeting(x,y,obj_player))

{
    by_door = 1;
    // if (keyboard_check(vk_up))
    if (keyboard_check(vk_down)) || (gamepad_button_check(0,gp_face2))
        {   
            obj_player.x = target_x;
            obj_player.y = target_y;
            room_goto(target_r);
        }
}

else

{
    by_door = 0;
}

