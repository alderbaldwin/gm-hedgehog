{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 258,
  "bbox_right": 408,
  "bbox_top": 472,
  "bbox_bottom": 571,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 640,
  "height": 640,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7e7d0d84-c79c-4281-b79e-849d35857303","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7e7d0d84-c79c-4281-b79e-849d35857303","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},"LayerId":{"name":"03d5a7a4-0ece-44f8-baf4-e22c1d181b70","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprTreeSwing","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},"resourceVersion":"1.0","name":"7e7d0d84-c79c-4281-b79e-849d35857303","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprTreeSwing","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0945d027-e369-45da-98f2-e2e2e7d537d8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7e7d0d84-c79c-4281-b79e-849d35857303","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 320,
    "yorigin": 640,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprTreeSwing","path":"sprites/sprTreeSwing/sprTreeSwing.yy",},
    "resourceVersion": "1.3",
    "name": "sprTreeSwing",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"03d5a7a4-0ece-44f8-baf4-e22c1d181b70","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprTreeSwing",
  "tags": [],
  "resourceType": "GMSprite",
}